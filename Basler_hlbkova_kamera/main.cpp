
#include <opencv2/opencv.hpp>
#include <windows.h>
#include <iostream>

using namespace cv;

// Include files to use the pylon API.
#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using cout.
using namespace std;
using namespace GenApi;

// Number of images to be grabbed.
static const uint32_t c_countOfImagesToGrab = 100;

void openBaslerCamera()
{
	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();

	try
	{
		// Create an instant camera object with the camera device found first.
		CInstantCamera camera(CTlFactory::GetInstance().CreateFirstDevice());

		// Print the model name of the camera.
		cout << "Using device " << camera.GetDeviceInfo().GetModelName() << endl;
	
		// The parameter MaxNumBuffer can be used to control the count of buffers
		// allocated for grabbing. The default value of this parameter is 10.
		camera.MaxNumBuffer = 1;

		//moznost nastavenia parametrov konzolou
		char zmena = 'n';
		cout << "Pre zmenu nastaveni stlac y" << endl;
		cin >> zmena;
		

		// Camera.StopGrabbing() is called automatically by the RetrieveResult() method
		// when c_countOfImagesToGrab images have been retrieved.
		while (true /*camera.IsGrabbing()*/)
		{
			
			// Start the grabbing of c_countOfImagesToGrab images.
			// The camera device is parameterized with a default configuration which
			// sets up free-running continuous acquisition.
			//camera.StartGrabbing(1);
			camera.StartGrabbing(1);
			GenApi::INodeMap& nodemap = camera.GetNodeMap();

			cout << "parametre kamery: " << endl;
			cout << "Sensor_width:" << CIntegerParameter(nodemap, "SensorWidth").GetValue() << endl;
			cout << "SensorHeight:" << CIntegerParameter(nodemap, "SensorHeight").GetValue() << endl;
			cout << "WidthMax:" << CIntegerParameter(nodemap, "WidthMax").GetValue() << endl;
			cout << "HeightMax:" << CIntegerParameter(nodemap, "HeightMax").GetValue() << endl;
			cout << "Width:" << CIntegerParameter(nodemap, "Width").GetValue() << endl;
			cout << "Height:" << CIntegerParameter(nodemap, "Height").GetValue() << endl;
			cout << "OffsetX:" << CIntegerParameter(nodemap, "OffsetX").GetValue() << endl;
			cout << "OffsetY:" << CIntegerParameter(nodemap, "OffsetY").GetValue() << endl;
			cout << "Min_Range:" << CIntegerParameter(nodemap, "WorkingRangeMin").GetValue() << endl;
			cout << "Max_Range:" << CIntegerParameter(nodemap, "WorkingRangeMax").GetValue() << endl;

			cout << "parametre nastavenia kamery: " << endl;
			cout << "Operation mode:" << CEnumParameter(nodemap, "OperatingMode").GetValue() << endl;
			cout << "Fast mode:" << CBooleanParameter(nodemap, "FastMode").GetValue() << endl;
			cout << "Exposure time:" << CFloatParameter(nodemap, "ExposureTime").GetValue() << endl;
			cout << "min depth:" << CIntegerParameter(nodemap, "DepthMin").GetValue() << endl;
			cout << "max depth:" << CIntegerParameter(nodemap, "DepthMax").GetValue() << endl;
			if (zmena == 'y') {
				int minDepth=0, maxDepth=1498, fastMode=0, modyIn=0;
				float exposureTime=0.0;
				cout << "zadaj min depth: ";
				cin >> minDepth;
				cout << endl;
				cout << "zadaj max depth: ";				
				cin >> maxDepth;
				cout << endl;
				
				cout << "Fast mode stlac 0 pre vyp, 1 pre zap: ";
				cin >> fastMode;
				cout << endl;
				cout << "stlac 0 pre Short Range, 1 pre Long Range mod: ";
				cin >> modyIn;
				cout << endl;
				CIntegerParameter(nodemap, "DepthMin").SetValue(minDepth);
				CIntegerParameter(nodemap, "DepthMax").SetValue(maxDepth);
				
				CBooleanParameter(nodemap, "FastMode").SetValue(fastMode);
				//v shortrange mode sa exposure time zmeni na 250microsekund a pri longrange sa zmeni na 1000micro sekund by default
				if(modyIn==0){ CEnumParameter(nodemap, "OperatingMode").SetValue("ShortRange"); }
				else if(modyIn==1){ CEnumParameter(nodemap, "OperatingMode").SetValue("LongRange"); }
				cout << "zadaj exposure time vo float cisle: ";
				cin >> exposureTime;
				cout << endl;
				CFloatParameter(nodemap, "ExposureTime").SetValue(exposureTime);
				cout << "nastavene hodnoty: " << endl;
				cout << "Operation mode:" << CEnumParameter(nodemap, "OperatingMode").GetValue() << endl;
				cout << "Fast mode:" << CBooleanParameter(nodemap, "FastMode").GetValue() << endl;
				cout << "Exposure time:" << CFloatParameter(nodemap, "ExposureTime").GetValue() << endl;
				cout << "min depth:" << CIntegerParameter(nodemap, "DepthMin").GetValue() << endl;
				cout << "max depth:" << CIntegerParameter(nodemap, "DepthMax").GetValue() << endl;
				cout << "pre pokracovanie stalcte n";
				cin >> zmena;
			}

			//zistenie vzdialenosti
			const double gray2mm = CFloatParameter(nodemap, "Scan3dCoordinateScale").GetValue();
			cout << "scale:" << gray2mm << endl;

			// This smart pointer will receive the grab result data.
			CGrabResultPtr ptrGrabResult;

			// Wait for an image and then retrieve it. A timeout of 5000 ms is used.
			camera.RetrieveResult(1000, ptrGrabResult, TimeoutHandling_ThrowException);

			Mat img;

			// Image grabbed successfully?
			if (ptrGrabResult->GrabSucceeded())
			{
				auto pixelType = ptrGrabResult->GetPixelType();

				//cout << "Pixel type: " << pixelType << endl;

				// For grayscale camera
				if (pixelType == Pylon::EPixelType::PixelType_Mono8)
				{
					// Access the image data.
					unsigned char* dataBuffer = (unsigned char*)ptrGrabResult->GetBuffer();

					// Create CV image 
					img = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_16UC1, dataBuffer, Mat::AUTO_STEP);
				}
				// For depth camera
				if (pixelType == Pylon::EPixelType::PixelType_Mono16)
				{
					
					img = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_16UC1, (uint8_t*)ptrGrabResult->GetBuffer());
				}
			}
			else
			{
				cout << "Pylog image grab Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
			}

			if (!img.empty())
			{	
				//rozdelenie obrazu na 2 casti definovane od zaciatocnej suradnicovej hodnoty po konecnu y-ovu
				//od zaciatocnej xovej po konecnu x-ovu
				Mat depth= img(cv::Range(0, 480), cv::Range(0, 640));
				//cv::imshow("Depth", depth);
				Mat ir= img(cv::Range(480, 960), cv::Range(0, 640));
				//konvertacia maticovych objektov na datove typy 1/1000.0=float, 1/255 = uint8
				depth.convertTo(depth, CV_32F, gray2mm);
				ir.convertTo(ir, CV_8U, 1 / 255.0);
				//zobrazenie obrazu kamery
				cv::imshow("Depth_f", depth);
				cv::imshow("Ir", ir);

				uchar irPixelValue = ir.at<uchar>(100, 100);
				//konverzia z uchar na uint
				uint irValue = (uint)irPixelValue;
				//depth pixel na suradnici y100 x100
				float depthPixelValue = (depth.at<float>(100, 100));
				//vypis
				cout <<"Ir: " << irValue << endl;
				cout << "Depth: " << depthPixelValue << endl;
				waitKey(1);
			}

			
		}
	}
	catch (const GenericException& e)
	{
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}

	// Releases all pylon resources. 
	PylonTerminate();
}



int main()
{

	openBaslerCamera();
}
